<?php

use Drupal\inxmail\Operations\Subscribe;
use Drupal\inxmail\Operations\Unsubscribe;

/**
 * URL: inxmail/api/subscribe
 *
 * @param email string email address to subscribe
 * @param list string name of the mailing list to subscribe to
 * @param source string (optional) Inxmail source identifier
 * @param attributes (optional) Inxmail subscriber attributes
 */
function webapi_inxmail_newsletter_registration_subscribe()
{
	$input = $_REQUEST;
	$content = array();
	$response = array(
		'status' => 'success',
	);

	try
	{
		$operation = new Subscribe($input['email'], $input['list'], isset($input['attributes']) ? $input['attributes'] : NULL, isset($input['source']) ? $input['source'] : 'Drupal subscription');
		$operation->execute();

		$content = array(
			'success' => array(
				'#theme' => 'inxmail_newsletter_registration_success',
				'#content' => variable_get_value('inxmail_registration_success_message'),
				'#parameters' => $input,
			),
		);
	}
	catch (Exception $e)
	{
		$response['status'] = 'error';
		$response['message'] = variable_get_value('inxmail_registration_error_message');
		$response['errorDetail'] = $e->getMessage();
		$response['errorFull'] = $e->__toString();

		$content = array(
			'success' => array(
				'#theme' => 'inxmail_newsletter_registration_error',
				'#message' => $response['message'],
				'#detail' => $response['errorDetail'],
			),
		);
	}

	$response['content'] = render($content);
	drupal_json_output($response);
}

/**
 * URL: inxmail/api/unsubscribe
 *
 * @param email string email address to unsubscribe
 * @param list string name of the mailing list to unsubscribe from
 * @param source string (optional) Inxmail source identifier
 */
function webapi_inxmail_newsletter_registration_unsubscribe()
{
	$input = $_REQUEST;
	$content = array();
	$response = array(
		'status' => 'success',
	);

	try
	{
		$operation = new Unsubscribe($input['email'], $input['list'], isset($input['source']) ? $input['source'] : 'Drupal subscription');
		$operation->execute();

		$content = array(
			'success' => array(
				'#theme' => 'inxmail_newsletter_registration_success',
				'#content' => variable_get_value('inxmail_registration_success_message'),
				'#parameters' => $input,
			),
		);
	}
	catch (Exception $e)
	{
		$response['status'] = 'error';
		$response['message'] = variable_get_value('inxmail_registration_error_message');
		$response['errorDetail'] = $e->getMessage();
		$response['errorFull'] = $e->__toString();

		$content = array(
			'success' => array(
				'#theme' => 'inxmail_newsletter_registration_error',
				'#message' => $response['message'],
				'#detail' => $response['errorDetail'],
			),
		);
	}

	$response['content'] = render($content);
	drupal_json_output($response);
}
