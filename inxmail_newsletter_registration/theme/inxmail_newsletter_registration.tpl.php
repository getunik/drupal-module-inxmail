
<div class="inxmail-newsletter-registration" data-list="<?php print $list_name; ?>" data-source="<?php print $source; ?>" data-attributes='<?php print $inx_attributes; ?>'>
	<div class="inxmail-newsletter-registration-form">
		<?php print render($content); ?>
	</div>
	<div style="display: none;" class="inxmail-newsletter-registration-error-container">
		<?php print render($validation_block); ?>
	</div>
	<?php print render($loading_block); ?>
</div>
