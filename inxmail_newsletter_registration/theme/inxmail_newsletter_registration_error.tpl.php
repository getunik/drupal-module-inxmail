
<div class="inxmail-newsletter-registration-error">
	<span class="inxmail-newsletter-registration-error-message">
		<?php print check_plain(isset($message) ? $message : ''); ?>
	</span>

	<?php if (!empty($detail)): ?>
		<span class="inxmail-newsletter-registration-error-detail">
			<?php print check_plain($detail); ?>
		</span>
	<?php endif; ?>
</div>
