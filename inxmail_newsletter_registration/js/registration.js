
(function ($) {

	// taken directly from the jQuery Validate source code (http://jqueryvalidation.org/)
	var emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;

	function isValidEmail(email) {
		return emailRegex.test(email);
	}

	Drupal.behaviors.inxmailRegistration = {
		attach: function(context, settings) {
			$('.inxmail-newsletter-registration', context).once('inxmailRegistration', function () {

				var registration = $(this),
					form = registration.find('.inxmail-newsletter-registration-form'),
					errorContainer = registration.find('.inxmail-newsletter-registration-error-container'),
					// the initial content of the error element is the validation message
					validationError = errorContainer.find('.inxmail-newsletter-registration-error').detach(),
					loading = registration.find('.inxmail-newsletter-registration-loading'),
					button = form.find('.inxmail-newsletter-registration-button'),
					input = form.find('.inxmail-newsletter-registration-email'),
					list = registration.data('list');

				function showError(errorContent) {
					errorContainer.empty().append(errorContent);
					errorContainer.show();
				}

				button.on('click', function (e) {
					e.preventDefault();

					var email = $.trim(input.val()),
						endpointUrl = settings.basePath + settings.pathPrefix + settings.inxmail.api.baseUrl + '/subscribe';

					if (isValidEmail(email)) {
						form.hide();
						loading.show();

						$.ajax(endpointUrl, {
							data: $.extend({}, registration.data(), { email: input.val() }),
						}).done(function(data) {
							loading.hide();
							var e = $.Event('inxmail-registration-complete');
							e.data = data;
							registration.trigger(e);

							if (e.isDefaultPrevented()) {
								form.show();
							} else {
								if (data.status === 'success') {
									e = $.Event('inxmail-registration-success');
									e.mailingList = list;

									registration.append($(data.content));
									registration.trigger(e);
								} else {
									showError($(data.content));
									form.show();
								}
							}
						}).fail(function() {
							loading.hide();
							form.show();

							var error = $('<span></span>').addClass('inxmail-newsletter-registration-error-message').text('An error occurred communicating with the server. Please try again later');
							showError(error);
						});
					} else {
						showError(validationError);
					}
				});

				input.on('keydown', function () {
					errorContainer.hide();
				});

			});
		}
	};

})(jQuery);
