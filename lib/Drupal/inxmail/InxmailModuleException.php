<?php

namespace Drupal\inxmail;

class InxmailModuleError
{
	const GENERIC = 0;
	const DUPLICATE_EMAIL = 1;
	const UNKNOWN_LIST = 2;
	const UNKNOWN_RECIPIENT = 3;
	const UNKNOWN_MAILING = 4;
}

class InxmailModuleException extends \Exception
{

}
