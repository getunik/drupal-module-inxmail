<?php

/*
 * Copyright(c) 2014, getunik AG (http://www.getunik.com)
 * ALL Rights Reserved
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of getunik AG and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to getunik AG and its suppliers and
 * may be covered by Swiss and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from getunik AG.
 */

namespace Drupal\inxmail\Operations;

/**
 *
 */
class DeleteRecipient extends \Drupal\inxmail\Operation
{
	private $recipientEmail;

	/**
	 *
	 */
	public function __construct($recipientEmail)
	{
		parent::__construct();

		$this->recipientEmail = $recipientEmail;
	}

	protected function executeOperation()
	{
		$find = new FindRecipient($this->recipientEmail);
		$recipientRowSet = $find->execute();

		if ($recipientRowSet === NULL)
		{
			return false;
		}
		else
		{
			$recipientRowSet->deleteRow();
		}
	}
}
