<?php

/*
 * Copyright(c) 2014, getunik AG (http://www.getunik.com)
 * ALL Rights Reserved
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of getunik AG and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to getunik AG and its suppliers and
 * may be covered by Swiss and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from getunik AG.
 */

namespace Drupal\inxmail\Operations;

/**
 *
 */
class Subscribe extends \Drupal\inxmail\Operation
{
	private $email;
	private $list;
	private $source;
	private $subscriberAttributes;

	/**
	 *
	 */
	public function __construct($email, $list, $subscriberAttributes = NULL, $source = NULL)
	{
		parent::__construct();

		$this->email = $email;
		$this->list = $list;
		$this->source = $source;
		$this->subscriberAttributes = $subscriberAttributes;
	}

	protected function executeOperation()
	{
		$listContext = $this->session->getListContext($this->list);

		$sessionManager = $this->session->getSubscriptionManager();
		return $sessionManager->processSubscription($this->source, $_SERVER['REMOTE_ADDR'], $listContext, $this->email, $this->subscriberAttributes);
	}
}
