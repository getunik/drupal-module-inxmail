<?php

/*
 * Copyright(c) 2014, getunik AG (http://www.getunik.com)
 * ALL Rights Reserved
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of getunik AG and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to getunik AG and its suppliers and
 * may be covered by Swiss and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from getunik AG.
 */

namespace Drupal\inxmail\Operations;

use Drupal\inxmail\InxmailModuleException;
use Drupal\inxmail\InxmailModuleError;

/**
 *
 */
class InstantMailSubscribe extends InstantMail
{
	private $subscriberAttributes;
	private $forceUpdate;

	/**
	 *
	 */
	public function __construct($recipientEmail, $list, $subscriberAttributes, $mailingId, $fromAddress = NULL, $isTriggerMailing = false, $forceUpdate = true)
	{
		parent::__construct($recipientEmail, $list, $mailingId, $fromAddress, $isTriggerMailing);

		$this->subscriberAttributes = $subscriberAttributes;
		$this->forceUpdate = $forceUpdate;
	}

	protected function executeOperation()
	{
		if ($this->forceUpdate)
		{
			$update = new CreateOrUpdateRecipient($this->recipientEmail, $this->subscriberAttributes);
			$update->execute();
		}

		$subscribe = new Subscribe($this->recipientEmail, $this->list, $this->subscriberAttributes);
		$subscribe->execute();

		return parent::executeOperation();
	}
}
