<?php

/*
 * Copyright(c) 2014, getunik AG (http://www.getunik.com)
 * ALL Rights Reserved
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of getunik AG and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to getunik AG and its suppliers and
 * may be covered by Swiss and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from getunik AG.
 */

namespace Drupal\inxmail\Operations;

use Drupal\inxmail\InxmailModuleException;
use Drupal\inxmail\InxmailModuleError;

/**
 * Updates an Inxmail recipient row set with the given recipient data attributes
 */
class CreateOrUpdateRecipient extends \Drupal\inxmail\Operation
{

	private $recipientEmail;
	private $recipientData;

	/**
	 *
	 */
	public function __construct($recipientEmail, $recipientData)
	{
		parent::__construct();

		$this->recipientEmail = $recipientEmail;
		$this->recipientData = $recipientData;
		$this->recipientData['email'] = $this->recipientEmail;
	}

	protected function executeOperation()
	{
		$find = new FindRecipient($this->recipientEmail);
		$recipientRowSet = $find->execute();
		if ($recipientRowSet === NULL)
		{
			$recipientRowSet = $this->session->getRecipientContext()->createRowSet();
			$recipientRowSet->moveToInsertRow();
		}

		$metadata = $recipientRowSet->getMetaData();

		foreach ($this->recipientData as $name => $value)
		{
			$attr = $metadata->getUserAttribute($name);
			$recipientRowSet->updateObject($attr, $value);
		}

		try
		{
			$recipientRowSet->commitRowUpdate();
		}
		catch (Inx_Api_Recipient_DuplicateKeyException $x)
		{
			throw new InxmailModuleException("Recipient update failed", InxmailModuleError::DUPLICATE_EMAIL, $x);
		}
		catch (Exception $x)
		{
			throw new InxmailModuleException("Recipient update failed", InxmailModuleError::GENERIC, $x);
		}

		$recipientRowSet->close();
	}
}
