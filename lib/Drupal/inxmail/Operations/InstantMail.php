<?php

/*
 * Copyright(c) 2014, getunik AG (http://www.getunik.com)
 * ALL Rights Reserved
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of getunik AG and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to getunik AG and its suppliers and
 * may be covered by Swiss and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from getunik AG.
 */

namespace Drupal\inxmail\Operations;

use Drupal\inxmail\InxmailModuleException;
use Drupal\inxmail\InxmailModuleError;

/**
 * Updates an Inxmail recipient row set with the given recipient data attributes
 */
class InstantMail extends \Drupal\inxmail\Operation
{

	protected $recipientEmail;
	protected $list;
	protected $mailingId;
	protected $fromAddress;
	protected $mailRetrievalMethod;

	/**
	 *
	 */
	public function __construct($recipientEmail, $list, $mailingId, $fromAddress = NULL, $isTriggerMailing = false)
	{
		parent::__construct();

		$this->recipientEmail = $recipientEmail;
		$this->list = $list;
		$this->mailingId = $mailingId;
		$this->fromAddress = $this->defaultValue($fromAddress, $this->session->getConfigValue('default_from_address'));

		$this->mailRetrievalMethod = 'getMail' . ($isTriggerMailing ? 'Trigger' : 'Normal');
	}

	protected function executeOperation()
	{
		$find = new FindRecipient($this->recipientEmail);
		$recipientRowSet = $find->execute();

		if ($recipientRowSet === NULL)
		{
			throw new InxmailModuleException('Cannot send instant mail to unknown recipient ' . $this->recipientEmail, InxmailModuleError::UNKNOWN_RECIPIENT);
		}

		$listContext = $this->session->getListContext($this->list);

		try
		{
			$mail = $this->{$this->mailRetrievalMethod}($recipientRowSet->getId());
		}
		catch (\Inx_Api_APIException $x)
		{
			throw new InxmailModuleException('Unable to retrieve mailing through ' . $this->mailRetrievalMethod . '. Maybe wrong mailing type for ID ' . $this->mailingId, InxmailModuleError::UNKNOWN_MAILING, $x);
		}

		$tempSender = $this->session->getTemporaryMailSender();
		$tempMail = $tempSender->createTemporaryMail($listContext);
		$tempMail->updateSubject($mail->getSubject());
		$tempMail->updateRecipientAddress($this->recipientEmail);

		if ($this->fromAddress !== NULL)
		{
			$tempMail->updateSenderAddress($this->fromAddress);
			$tempMail->updateReplyToAddress($this->fromAddress);
		}

		$tempMail->setContentHandler('Inx_Api_Mailing_HtmlTextContentHandler');
		$contentHandler = $tempMail->getContentHandler();
		$contentHandler->updateContent($mail->getHtmlText());

		$result = $tempSender->sendTemporaryMail($tempMail);


		return $result;
	}

	protected function getMailNormal($recipientId)
	{
		$mailingManager = $this->session->getMailingManager();
		$mailingRenderer = $mailingManager->createRenderer();

		// What we know about the different build modes
		// - BUILD_MODE_NORMAL does not embed embedded images
		// - BUILD_MODE_ALTERNATIVEVIEW_ACTIVE also does not embed embedded images, but at least it uses remote images on inxmail.com that actually show up
		// currently using BUILD_MODE_ALTERNATIVEVIEW_ACTIVE because it was the first one that actually "worked" from the recipient's point of view
		$mailingRenderer->parse($this->mailingId, \Inx_Api_Mail_MailingRenderer::BUILD_MODE_ALTERNATIVEVIEW_ACTIVE);
		return $mailingRenderer->build($recipientId);
	}

	protected function getMailTrigger($recipientId)
	{
		$triggerMailingManager = $this->session->getTriggerMailingManager();
		$mailingRenderer = $triggerMailingManager->createRenderer();

		// @see getMailNormal for information about build modes
		$mailingRenderer->parse($this->mailingId, \Inx_Api_TriggerMail_BuildMode::ALTERNATIVEVIEW_ACTIVE());
		return $mailingRenderer->build($recipientId);
	}
}
