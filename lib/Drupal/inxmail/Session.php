<?php

/*
 * Copyright(c) 2014, getunik AG (http://www.getunik.com)
 * ALL Rights Reserved
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of getunik AG and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to getunik AG and its suppliers and
 * may be covered by Swiss and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from getunik AG.
 */

namespace Drupal\inxmail;

/**
 *
 */
class Session
{
	private static $session = NULL;

	public static function get()
	{
		if (null === self::$session) {
			self::$session = new Session();
		}
		return self::$session;
	}

	private $url;
	private $user;
	private $pwd;
	private $inxSession;
	private $recipientContext;
	private $subscriptionManager;
	private $mailingManager;
	private $triggerMailingManager;
	private $temporaryMailSender;

	private function __construct()
	{
		$this->config = inxmail_configuration();
	}

	private function __clone(){}

	public function isOpen()
	{
		return isset($this->inxSession);
	}

	public function open()
	{
		if (!$this->isOpen())
		{
			$this->inxSession = \Inx_Api_Session::createRemoteSession($this->config['api_url'], $this->config['api_username'], $this->config['api_password']);
		}
	}

	public function close()
	{
		if ($this->isOpen())
		{
			$tmp = $this->inxSession;
			$this->inxSession = NULL;
			self::$session = NULL;
			$tmp->close();
		}
	}

	public function getConfigValue($name)
	{
		return $this->config[$name];
	}

	public function getRecipientContext()
	{
		if (!isset($this->recipientContext))
		{
			$this->recipientContext = $this->inxSession->createRecipientContext();
		}

		return $this->recipientContext;
	}

	public function getSubscriptionManager()
	{
		if (!isset($this->subscriptionManager))
		{
			$this->subscriptionManager = $this->inxSession->getSubscriptionManager();
		}

		return $this->subscriptionManager;
	}

	public function getListContext($listName)
	{
		$list = $this->inxSession->getListContextManager()->findByName($listName);

		if ($list == NULL)
		{
			throw new InxmailModuleException('Unable to find mailing list with name \'' . $listName . '\'', InxmailModuleError::UNKNOWN_LIST);
		}

		return $list;
	}

	public function getMailingManager()
	{
		if (!isset($this->mailingManager))
		{
			$this->mailingManager = $this->inxSession->getMailingManager();
		}

		return $this->mailingManager;
	}

	public function getTriggerMailingManager()
	{
		if (!isset($this->triggerMailingManager))
		{
			$this->triggerMailingManager = $this->inxSession->getTriggerMailingManager();
		}

		return $this->triggerMailingManager;
	}

	public function getTemporaryMailSender()
	{
		if (!isset($this->temporaryMailSender))
		{
			$this->temporaryMailSender = $this->inxSession->getTemporaryMailSender();
		}

		return $this->temporaryMailSender;
	}
}
