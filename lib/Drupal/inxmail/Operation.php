<?php

/*
 * Copyright(c) 2014, getunik AG (http://www.getunik.com)
 * ALL Rights Reserved
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of getunik AG and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to getunik AG and its suppliers and
 * may be covered by Swiss and Foreign Patents, patents in
 * process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from getunik AG.
 */

namespace Drupal\inxmail;

/**
 *
 */
class Operation
{
	protected $session;

	public function __construct()
	{
		$this->session = Session::get();
	}

	public function execute()
	{
		if ($this->session->isOpen())
		{
			$result = $this->executeOperation();
		}
		else
		{
			try {
				$this->session->open();

				$result = $this->executeOperation();

				$this->session->close();
			} catch(Exception $e) {
				watchdog('inxmail', 'Inxmail operation failed: '. $e->__toString(), null, WATCHDOG_ERROR);
				$this->session->close();
				throw $e;
			}
		}

		return $result;
	}

	protected function executeOperation()
	{}

	protected function defaultValue()
	{
		foreach (func_get_args() as $arg)
		{
			if (isset($arg) && $arg !== NULL)
			{
				return $arg;
			}
		}

		return NULL;
	}
}
